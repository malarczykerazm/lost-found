/* ############################## ADDRESSES ##############################  */
insert into addresses (address_number, street, postcode, city, country) values ('1', 'Kirkmanshulme Ln', 'M12 4WB', 'Manchester', 'UNITED_KINGDOM');
insert into addresses (address_number, street, postcode, city, country) values ('35', 'al. Ignacego Jana Paderewskiego', '50-001', 'Wrocław', 'POLAND');
insert into addresses (address_number, street, postcode, city, country) values ('47', 'Vinnerstadsvägen', '591 45', 'Motala', 'SWEDEN');
insert into addresses (address_number, street, postcode, city, country) values ('88', 'Fair Dr', 'CA 92626', 'Costa Mesa', 'UNITED_STATES');
insert into addresses (address_number, street, postcode, city, country) values ('1', 'Appelhäger Chaussee', '17166', 'Teterow', 'GERMANY');
insert into addresses (address_number, street, postcode, city, country) values ('76', 'Rye Rd', 'EN11 0EH', 'Hoddeson', 'UNITED_KINGDOM');
insert into addresses (address_number, street, postcode, city, country) values ('377', 'Alwalton St', 'PE2 0XE', 'Peterborough', 'UNITED_KINGDOM');
insert into addresses (address_number, street, postcode, city, country) values ('45', 'Rugby Rd', 'CV8 3GJ', 'Coventry', 'UNITED_KINGDOM');
insert into addresses (address_number, street, postcode, city, country) values ('23', 'Sutherland Av', 'WV2 2JJ', 'Wolverhampton', 'UNITED_KINGDOM');
insert into addresses (address_number, street, postcode, city, country) values ('54', 'Penistone Rd', 'S6 2DE', 'Sheffield', 'UNITED_KINGDOM');
insert into addresses (address_number, street, postcode, city, country) values ('18', 'Fossway Rd', 'NE6 2XJ', 'Newcastle', 'UNITED_KINGDOM');
insert into addresses (address_number, street, postcode, city, country) values ('82', 'Hawthorn St', 'G22 6RU', 'Glasgow', 'UNITED_KINGDOM');
insert into addresses (address_number, street, postcode, city, country) values ('16A', 'Abbey Road', 'NW8 9BD', 'London', 'UNITED_KINGDOM');
insert into addresses (address_number, street, postcode, city, country) values ('443', 'Aldborough Road South', 'IG3 8JN', 'Ilford', 'UNITED_KINGDOM');
insert into addresses (address_number, street, postcode, city, country) values ('7', 'Wells Road', 'BS27 3SW', 'Cheddar', 'UNITED_KINGDOM');

/* ############################## CONTACT_DATA ##############################  */
insert into contact_data (phone, email) values ('970-(273)893-7227', 'mstarr0@nyu.edu');
insert into contact_data (phone, email) values ('352-(744)301-2884', 'hpingston1@pcworld.com');
insert into contact_data (phone, email) values ('63-(479)525-3679', 'ndamsell2@hibu.com');
insert into contact_data (phone, email) values ('251-(293)307-7736', 'gondrousek3@bing.com');
insert into contact_data (phone, email) values ('234-(679)596-9980', 'rsaull4@reddit.com');
insert into contact_data (phone, email) values ('62-(298)106-3961', 'tpetric5@paginegialle.it');
insert into contact_data (phone, email) values ('7-(527)630-6569', 'fjordine6@stumbleupon.com');
insert into contact_data (phone, email) values ('420-(566)699-5516', 'gsimkovitz7@adobe.com');
insert into contact_data (phone, email) values ('62-(711)475-6836', 'kbragg8@fda.gov');
insert into contact_data (phone, email) values ('63-(934)370-6809', 'tdeeman9@bbb.org');
insert into contact_data (phone, email) values ('86-(201)568-9631', 'dchilda@unc.edu');
insert into contact_data (phone, email) values ('55-(162)993-6891', 'rburgessb@godaddy.com');
insert into contact_data (phone, email) values ('86-(828)908-4159', 'dlippingwellc@java.com');
insert into contact_data (phone, email) values ('992-(338)918-7815', 'bklaggemand@japanpost.jp');
insert into contact_data (phone, email) values ('86-(683)662-4192', 'lchildee@bloomberg.com');
insert into contact_data (phone, email) values ('7-(569)348-5493', 'ndowdf@gnu.org');
insert into contact_data (phone, email) values ('86-(822)958-3831', 'gfirbyg@discuz.net');

/* ############################## CLIENTS ##############################  */
insert into clients (firstname, surname, social_security_number, address_id, contact_data_id) values ('Camile', 'Rigmand', '58968240902', 1, 1);
insert into clients (firstname, surname, social_security_number, address_id, contact_data_id) values ('Prudy', 'Blower', '63764585506', 2, 2);
insert into clients (firstname, surname, social_security_number, address_id, contact_data_id) values ('Had', 'Speare', '93350245101', 3, 3);
insert into clients (firstname, surname, social_security_number, address_id, contact_data_id) values ('Goldy', 'Maginn', '94133601602', 4, 4);
insert into clients (firstname, surname, social_security_number, address_id, contact_data_id) values ('Robinia', 'Scyone', '61073653003', 5, 5);
insert into clients (firstname, surname, social_security_number, address_id, contact_data_id) values ('Farlie', 'Whitlam', '50061949807', 6, 6);
insert into clients (firstname, surname, social_security_number, address_id, contact_data_id) values ('Erinna', 'Frift', '71286640303', 7, 7);
insert into clients (firstname, surname, social_security_number, address_id, contact_data_id) values ('Craggy', 'Novotna', '75937974900', 8, 8);
insert into clients (firstname, surname, social_security_number, address_id, contact_data_id) values ('Ingemar', 'Broadbent', '77400084403', 9, 9);
insert into clients (firstname, surname, social_security_number, address_id, contact_data_id) values ('Audre', 'Heimes', '46906336300', 10, 10);
insert into clients (firstname, surname, social_security_number, address_id, contact_data_id) values ('Gustaf', 'Parsonson', '93358426702', 11, 11);
insert into clients (firstname, surname, social_security_number, address_id, contact_data_id) values ('Clio', 'Wieprecht', '64132354406', 11, 16);
insert into clients (firstname, surname, social_security_number, address_id, contact_data_id) values ('Petra', 'Sprigging', '75984640703', 12, 12);
insert into clients (firstname, surname, social_security_number, address_id, contact_data_id) values ('Amalia', 'Surgey', '42424128000', 13, 13);
insert into clients (firstname, surname, social_security_number, address_id, contact_data_id) values ('Myrtia', 'Leather', '73606269207', 14, 14);
insert into clients (firstname, surname, social_security_number, address_id, contact_data_id) values ('Zachery', 'Noyce', '41731410009', 14, 17);
insert into clients (firstname, surname, social_security_number, address_id, contact_data_id) values ('Dara', 'Goody', '29429303104', 15, 15);

/* ############################## FOUND_ITEMS ##############################  */
insert into found_items (item_name, category, found_when, found_where, colour, weight, reported_by_client, status, out_of_deposit_since, given_to_client) values ('Long coat', 'CLOTHES', '2017-08-09', 'Challans, near the fountain', 'RED', 2.5, 1, 'RETURNED_TO_OWNER', '2017-08-12', 16);
insert into found_items (item_name, category, found_when, found_where, colour, weight, reported_by_client, status, out_of_deposit_since, given_to_client) values ('Short Cout', 'CLOTHES', '2017-05-02', 'Pavlodol’skaya, near the town hall', 'BLUE', 1.4, 2, 'RETURNED_TO_OWNER', '2017-05-04', 17);
insert into found_items (item_name, category, found_when, found_where, colour, weight, status, out_of_deposit_since, given_to_client) values ('Backpack', 'BAG', '2017-03-15', 'Fengsheng, the old city', 'YELLOW', 8.0, 'GIVEN_AWAY', '2017-05-31', 15);
insert into found_items (item_name, category, found_when, found_where, colour, weight, reported_by_client, status) values ('Visa Credit Card', 'CREDIT_OR_DEBIT_CARD', '2017-09-05', 'Pingyang, the old city', 'GRAY', 0.1, 4, 'IN_DEPOSIT');
insert into found_items (item_name, category, found_when, found_where, colour, weight, reported_by_client, status) values ('Big touristic bag', 'BAG', '2017-03-03', 'Arlington, near the fountain', 'GRAY', 3.3, 5, 'IN_DEPOSIT');
insert into found_items (item_name, category, found_when, found_where, colour, weight, reported_by_client, status) values ('Tooth brush', 'PERSONAL_ITEM', '2017-02-14', 'Wongaya Kaja, near the fountain', 'RED', 0.1, 6, 'IN_DEPOSIT');
insert into found_items (item_name, category, found_when, found_where, colour, weight, reported_by_client, status) values ('MasterCard Credit Card', 'CREDIT_OR_DEBIT_CARD', '2017-06-04', 'Huajialing, old city', 'ORANGE', 0.1, 7, 'IN_DEPOSIT');
insert into found_items (item_name, category, found_when, found_where, colour, weight, status) values ('ID', 'DOCUMENT', '2017-07-12', 'Baranti, near the fountain', 'BROWN', 0.1, 'IN_DEPOSIT');
insert into found_items (item_name, category, found_when, found_where, colour, weight, reported_by_client, status) values ('Gloves', 'CLOTHES', '2017-05-21', 'Gaopeng, near the fountain', 'BLUE', 0.3, 9, 'IN_DEPOSIT');
insert into found_items (item_name, category, found_when, found_where, colour, weight, status) values ('Passport', 'DOCUMENT', '2017-01-14', 'Chocope, city centre', 'YELLOW', 0.2, 'IN_DEPOSIT');
insert into found_items (item_name, category, found_when, found_where, colour, weight, reported_by_client, status) values ('Passport', 'DOCUMENT', '2017-03-26', 'Ranchuelo, downtown', 'GRAY', 0.1, 11, 'IN_DEPOSIT');
insert into found_items (item_name, category, found_when, found_where, colour, weight, reported_by_client, status) values ('Big umbrella', 'UMBRELLA', '2017-04-17', 'Samos, downtown', 'ORANGE', 0.8, 12, 'IN_DEPOSIT');
insert into found_items (item_name, category, found_when, found_where, colour, weight, additional_description, reported_by_client, status) values ('Mountain bike', 'BIKE', '2017-04-16', 'Mentaras, CraigLee Dr', 'GOLDEN', 25.0, 'Just a nice bike.', 13, 'IN_DEPOSIT');
insert into found_items (item_name, category, found_when, found_where, colour, weight, reported_by_client, status) values ('Visa Debit Card', 'CREDIT_OR_DEBIT_CARD', '2017-01-28', 'Turku, city centre', 'GRAY', 0.1, 14, 'IN_DEPOSIT');

/* ############################## LOST_ITEMS ##############################  */
insert into lost_items (item_name, category, lost_when, lost_where, colour, weight, reported_by_client, is_deal_completed, recognized_as_found_item) values ('Long coat', 'CLOTHES', '2017-08-11', 'Challans, close to the fountain', 'RED', 2.0, 16, TRUE, 1);
insert into lost_items (item_name, category, lost_when, lost_where, colour, weight, reported_by_client, is_deal_completed, recognized_as_found_item) values ('Jacket', 'CLOTHES', '2017-05-01', 'Pavlodol’skaya, the old city', 'BLUE', 1.0, 17, TRUE, 2);
insert into lost_items (item_name, category, lost_when, lost_where, colour, weight, reported_by_client, is_deal_completed) values ('Backpack', 'BAG', '2017-02-12', 'New York, Manhattan', 'YELLOW', 5.0, 15, FALSE);
insert into lost_items (item_name, category, lost_when, lost_where, colour, weight, reported_by_client, is_deal_completed) values ('Gloves', 'CLOTHES', '2017-01-14', 'Wrocław, Rynek', 'BLACK', 0.3, 2, FALSE);
insert into lost_items (item_name, category, lost_when, lost_where, colour, weight, additional_description,  reported_by_client, is_deal_completed) values ('Gold bike', 'BIKE', '2017-05-15', 'Berlin, downtown', 'GOLDEN', 12.0, 'Real gold.', 5, FALSE);
insert into lost_items (item_name, category, lost_when, lost_where, colour, weight, reported_by_client, is_deal_completed) values ('Leather wallet', 'WALLET', '2017-08-29', 'Ranchuelo, downtown', 'BROWN', 0.3, 8, FALSE);
insert into lost_items (item_name, category, lost_when, lost_where, colour, weight, reported_by_client, is_deal_completed) values ('ID card', 'DOCUMENT', '2017-09-01', 'Ranchuelo, downtown', 'GRAY', 0.1, 11, FALSE);
insert into lost_items (item_name, category, lost_when, lost_where, colour, weight, reported_by_client, is_deal_completed) values ('ID card', 'DOCUMENT', '2017-09-02', 'London, downtown', 'GRAY', 0.1, 14, FALSE);
insert into lost_items (item_name, category, lost_when, lost_where, colour, weight, reported_by_client, is_deal_completed) values ('Umbrella', 'UMBRELLA', '2017-08-12', 'Edinburgh, the Royal Mile', 'PINK', 0.5, 7, FALSE);
insert into lost_items (item_name, category, lost_when, lost_where, colour, weight, reported_by_client, is_deal_completed) values ('Big umbrella', 'UMBRELLA', '2017-08-12', 'Edinburgh, the Meadows', 'ORANGE', 0.4, 5, FALSE);
insert into lost_items (item_name, category, lost_when, lost_where, colour, weight, additional_description, reported_by_client, is_deal_completed) values ('Little umbrella', 'UMBRELLA', '2017-09-02', 'Wrocław, business garden', 'ORANGE', 0.2, 'Very small umbrella.', 13, FALSE);

/* ############################## CLIENT_ITEM_WISHES ##############################  */
insert into client_item_wishes (client_id, found_item_id, client_rating) values (1, 4, 1);
insert into client_item_wishes (client_id, found_item_id, client_rating) values (2, 4, 2);
insert into client_item_wishes (client_id, found_item_id, client_rating) values (3, 4, 3);
insert into client_item_wishes (client_id, found_item_id, client_rating) values (8, 4, 5);
insert into client_item_wishes (client_id, found_item_id, client_rating) values (5, 4, 5);
insert into client_item_wishes (client_id, found_item_id, client_rating) values (8, 6, 0);
insert into client_item_wishes (client_id, found_item_id, client_rating) values (9, 6, 1);
insert into client_item_wishes (client_id, found_item_id, client_rating) values (11, 7, 1);
insert into client_item_wishes (client_id, found_item_id, client_rating) values (12, 7, 4);
insert into client_item_wishes (client_id, found_item_id, client_rating) values (14, 7, 4);
insert into client_item_wishes (client_id, found_item_id, client_rating) values (1, 8, 3);
insert into client_item_wishes (client_id, found_item_id, client_rating) values (1, 9, 5);
insert into client_item_wishes (client_id, found_item_id, client_rating) values (2, 10, 5);
insert into client_item_wishes (client_id, found_item_id, client_rating) values (2, 11, 3);
insert into client_item_wishes (client_id, found_item_id, client_rating) values (4, 12, 4);
insert into client_item_wishes (client_id, found_item_id, client_rating) values (5, 12, 4);
insert into client_item_wishes (client_id, found_item_id, client_rating) values (9, 12, 2);
insert into client_item_wishes (client_id, found_item_id, client_rating) values (15, 13, 2);
insert into client_item_wishes (client_id, found_item_id, client_rating) values (15, 14, 5);
insert into client_item_wishes (client_id, found_item_id, client_rating) values (16, 14, 5);
insert into client_item_wishes (client_id, found_item_id, client_rating) values (17, 14, 5);
