package com.capgemini.mapper;

import com.capgemini.domain.ClientEntity;
import com.capgemini.to.ClientTO;

public class ClientMapper {

	public static ClientTO map(ClientEntity entity) {
		ClientTO to = new ClientTO();

		to.setID(entity.getID());
		to.setVersion(entity.getVersion());
		to.setCreationTime(entity.getCreationTime());
		to.setModificationTime(entity.getModificationTime());

		if (null != entity.getFirstname()) {
			to.setFirstname(entity.getFirstname());
		}
		if (null != entity.getSurname()) {
			to.setSurname(entity.getSurname());
		}
		if (null != entity.getSocialSecurityNumber()) {
			to.setSocialSecurityNumber(entity.getSocialSecurityNumber());
		}
		if (null != entity.getAddress()) {
			to.setAddress(AddressMapper.map(entity.getAddress()));
		}
		if (null != entity.getContactData()) {
			to.setContactData(ContactDataMapper.map(entity.getContactData()));
		}

		return to;
	}

	public static ClientEntity map(ClientTO to) {
		ClientEntity entity = new ClientEntity();

		entity.setID(to.getID());
		entity.setVersion(to.getVersion());
		entity.setCreationTime(to.getCreationTime());
		entity.setModificationTime(to.getModificationTime());

		if (null != to.getFirstname()) {
			entity.setFirstname(to.getFirstname());
		}
		if (null != to.getSurname()) {
			entity.setSurname(to.getSurname());
		}
		if (null != to.getSocialSecurityNumber()) {
			entity.setSocialSecurityNumber(to.getSocialSecurityNumber());
		}
		if (null != to.getAddress()) {
			entity.setAddress(AddressMapper.map(to.getAddress()));
		}
		if (null != to.getContactData()) {
			entity.setContactData(ContactDataMapper.map(to.getContactData()));
		}

		return entity;
	}

}
