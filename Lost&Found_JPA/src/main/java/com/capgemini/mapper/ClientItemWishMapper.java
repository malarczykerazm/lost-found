package com.capgemini.mapper;

import com.capgemini.domain.ClientItemWishEntity;
import com.capgemini.to.ClientItemWishTO;

public class ClientItemWishMapper {

	public static ClientItemWishTO map(ClientItemWishEntity entity) {
		ClientItemWishTO to = new ClientItemWishTO();

		to.setID(entity.getID());
		to.setVersion(entity.getVersion());
		to.setCreationTime(entity.getCreationTime());
		to.setModificationTime(entity.getModificationTime());

		to.setClient(ClientMapper.map(entity.getClient()));
		to.setFoundItem(FoundItemMapper.map(entity.getFoundItem()));
		to.setClientRating(entity.getClientRating());

		return to;
	}

	public static ClientItemWishEntity map(ClientItemWishTO to) {
		ClientItemWishEntity entity = new ClientItemWishEntity();

		entity.setID(to.getID());
		entity.setVersion(to.getVersion());
		entity.setCreationTime(to.getCreationTime());
		entity.setModificationTime(to.getModificationTime());

		entity.setClient(ClientMapper.map(to.getClient()));
		entity.setFoundItem(FoundItemMapper.map(to.getFoundItem()));
		entity.setClientRating(to.getClientRating());

		return entity;
	}

}
