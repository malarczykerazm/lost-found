package com.capgemini.mapper;

import com.capgemini.domain.FoundItemEntity;
import com.capgemini.to.FoundItemTO;

public class FoundItemMapper {

	public static FoundItemTO map(FoundItemEntity entity) {
		FoundItemTO to = new FoundItemTO();

		to.setID(entity.getID());
		to.setVersion(entity.getVersion());
		to.setCreationTime(entity.getCreationTime());
		to.setModificationTime(entity.getModificationTime());

		if (null != entity.getItemName()) {
			to.setItemName(entity.getItemName());
		}
		if (null != entity.getCategory()) {
			to.setCategory(entity.getCategory());
		}
		if (null != entity.getFoundWhen()) {
			to.setFoundWhen(entity.getFoundWhen());
		}
		if (null != entity.getFoundWhere()) {
			to.setFoundWhere(entity.getFoundWhere());
		}
		if (null != entity.getColour()) {
			to.setColour(entity.getColour());
		}
		if (null != entity.getWeight()) {
			to.setWeight(entity.getWeight());
		}
		if (null != entity.getAddDesc()) {
			to.setAddDesc(entity.getAddDesc());
		}
		if (null != entity.getReportedByClient()) {
			to.setReportedByClient(ClientMapper.map(entity.getReportedByClient()));
		}
		if (null != entity.getStatus()) {
			to.setStatus(entity.getStatus());
		}
		if (null != entity.getOutOfDepositSince()) {
			to.setOutOfDepositSince(entity.getOutOfDepositSince());
		}
		if (null != entity.getGivenToClient()) {
			to.setGivenToClient(ClientMapper.map(entity.getGivenToClient()));
		}

		return to;
	}

	public static FoundItemEntity map(FoundItemTO to) {
		FoundItemEntity entity = new FoundItemEntity();

		entity.setID(to.getID());
		entity.setVersion(to.getVersion());
		entity.setCreationTime(to.getCreationTime());
		entity.setModificationTime(to.getModificationTime());

		if (null != to.getItemName()) {
			entity.setItemName(to.getItemName());
		}
		if (null != to.getCategory()) {
			entity.setCategory(to.getCategory());
		}
		if (null != to.getFoundWhen()) {
			entity.setFoundWhen(to.getFoundWhen());
		}
		if (null != to.getFoundWhere()) {
			entity.setFoundWhere(to.getFoundWhere());
		}
		if (null != to.getColour()) {
			entity.setColour(to.getColour());
		}
		if (null != to.getWeight()) {
			entity.setWeight(to.getWeight());
		}
		if (null != to.getAddDesc()) {
			entity.setAddDesc(to.getAddDesc());
		}
		if (null != to.getReportedByClient()) {
			entity.setReportedByClient(ClientMapper.map(to.getReportedByClient()));
		}
		if (null != to.getStatus()) {
			entity.setStatus(to.getStatus());
		}
		if (null != to.getOutOfDepositSince()) {
			entity.setOutOfDepositSince(to.getOutOfDepositSince());
		}
		if (null != to.getGivenToClient()) {
			entity.setGivenToClient(ClientMapper.map(to.getGivenToClient()));
		}

		return entity;
	}

}
