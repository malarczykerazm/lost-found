package com.capgemini.mapper;

import com.capgemini.domain.ContactDataEntity;
import com.capgemini.to.ContactDataTO;

public class ContactDataMapper {

	public static ContactDataTO map(ContactDataEntity entity) {
		ContactDataTO to = new ContactDataTO();

		to.setID(entity.getID());
		to.setVersion(entity.getVersion());
		to.setCreationTime(entity.getCreationTime());
		to.setModificationTime(entity.getModificationTime());

		to.setPhone(entity.getPhone());
		to.setEmail(entity.getEmail());

		return to;
	}

	public static ContactDataEntity map(ContactDataTO to) {
		ContactDataEntity entity = new ContactDataEntity();

		entity.setID(to.getID());
		entity.setVersion(to.getVersion());
		entity.setCreationTime(to.getCreationTime());
		entity.setModificationTime(to.getModificationTime());

		entity.setPhone(to.getPhone());
		entity.setEmail(to.getEmail());

		return entity;
	}

}
