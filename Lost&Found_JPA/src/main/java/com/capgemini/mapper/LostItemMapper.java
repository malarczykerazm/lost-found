package com.capgemini.mapper;

import com.capgemini.domain.LostItemEntity;
import com.capgemini.to.LostItemTO;

public class LostItemMapper {

	public static LostItemTO map(LostItemEntity entity) {
		LostItemTO to = new LostItemTO();

		to.setID(entity.getID());
		to.setVersion(entity.getVersion());
		to.setCreationTime(entity.getCreationTime());
		to.setModificationTime(entity.getModificationTime());

		if (null != entity.getItemName()) {
			to.setItemName(entity.getItemName());
		}
		if (null != entity.getCategory()) {
			to.setCategory(entity.getCategory());
		}
		if (null != entity.getLostWhen()) {
			to.setLostWhen(entity.getLostWhen());
		}
		if (null != entity.getLostWhere()) {
			to.setLostWhere(entity.getLostWhere());
		}
		if (null != entity.getColour()) {
			to.setColour(entity.getColour());
		}
		if (null != entity.getWeight()) {
			to.setWeight(entity.getWeight());
		}
		if (null != entity.getAddDesc()) {
			to.setAddDesc(entity.getAddDesc());
		}
		if (null != entity.getReportedByClient()) {
			to.setReportedByClient(ClientMapper.map(entity.getReportedByClient()));
		}
		if (null != entity.getIsDealCompleted()) {
			to.setIsDealCompleted(entity.getIsDealCompleted());
		}
		if (null != entity.getRecognizedAsFoundItem()) {
			to.setRecognizedAsFoundItem(FoundItemMapper.map(entity.getRecognizedAsFoundItem()));
		}

		return to;
	}

	public static LostItemEntity map(LostItemTO to) {
		LostItemEntity entity = new LostItemEntity();

		entity.setID(to.getID());
		entity.setVersion(to.getVersion());
		entity.setCreationTime(to.getCreationTime());
		entity.setModificationTime(to.getModificationTime());

		if (null != to.getItemName()) {
			entity.setItemName(to.getItemName());
		}
		if (null != to.getCategory()) {
			entity.setCategory(to.getCategory());
		}
		if (null != to.getLostWhen()) {
			entity.setLostWhen(to.getLostWhen());
		}
		if (null != to.getLostWhere()) {
			entity.setLostWhere(to.getLostWhere());
		}
		if (null != to.getColour()) {
			entity.setColour(to.getColour());
		}
		if (null != to.getWeight()) {
			entity.setWeight(to.getWeight());
		}
		if (null != to.getAddDesc()) {
			entity.setAddDesc(to.getAddDesc());
		}
		if (null != to.getReportedByClient()) {
			entity.setReportedByClient(ClientMapper.map(to.getReportedByClient()));
		}
		if (null != to.getIsDealCompleted()) {
			entity.setIsDealCompleted(to.getIsDealCompleted());
		}
		if (null != to.getRecognizedAsFoundItem()) {
			entity.setRecognizedAsFoundItem(FoundItemMapper.map(to.getRecognizedAsFoundItem()));
		}

		return entity;
	}

}
