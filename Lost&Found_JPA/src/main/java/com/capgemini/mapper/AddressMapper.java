package com.capgemini.mapper;

import com.capgemini.domain.AddressEntity;
import com.capgemini.to.AddressTO;

public class AddressMapper {

	public static AddressTO map(AddressEntity entity) {
		AddressTO to = new AddressTO();

		to.setID(entity.getID());
		to.setVersion(entity.getVersion());
		to.setCreationTime(entity.getCreationTime());
		to.setModificationTime(entity.getModificationTime());

		to.setAddressNumber(entity.getAddressNumber());
		to.setStreet(entity.getStreet());
		to.setPostcode(entity.getPostcode());
		to.setCity(entity.getCity());
		to.setCountry(entity.getCountry());

		return to;
	}

	public static AddressEntity map(AddressTO to) {
		AddressEntity entity = new AddressEntity();

		entity.setID(to.getID());
		entity.setVersion(to.getVersion());
		entity.setCreationTime(to.getCreationTime());
		entity.setModificationTime(to.getModificationTime());

		entity.setAddressNumber(to.getAddressNumber());
		entity.setStreet(to.getStreet());
		entity.setPostcode(to.getPostcode());
		entity.setCity(to.getCity());
		entity.setCountry(to.getCountry());

		return entity;
	}

}
