package com.capgemini.to;

public class ClientTO extends AbstractTO {

	private static final long serialVersionUID = -3602910875419215267L;

	private String firstname;

	private String surname;

	private String socialSecurityNumber;

	private AddressTO address;

	private ContactDataTO contactData;

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getSocialSecurityNumber() {
		return socialSecurityNumber;
	}

	public void setSocialSecurityNumber(String socialSecurityNumber) {
		this.socialSecurityNumber = socialSecurityNumber;
	}

	public AddressTO getAddress() {
		return address;
	}

	public void setAddress(AddressTO address) {
		this.address = address;
	}

	public ContactDataTO getContactData() {
		return contactData;
	}

	public void setContactData(ContactDataTO contactData) {
		this.contactData = contactData;
	}

}
