package com.capgemini.to;

import java.sql.Date;

import com.capgemini.enumerator.ItemCategory;
import com.capgemini.enumerator.ItemColour;

public class LostItemTO extends AbstractTO {

	private static final long serialVersionUID = -4462269841818097740L;

	private String itemName;

	private ItemCategory category;

	private Date lostWhen;

	private String lostWhere;

	private ItemColour colour;

	private Double weight;

	private String addDesc;

	private ClientTO reportedByClient;

	private Boolean isDealCompleted;

	private FoundItemTO recognizedAsFoundItem;

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public ItemCategory getCategory() {
		return category;
	}

	public void setCategory(ItemCategory category) {
		this.category = category;
	}

	public Date getLostWhen() {
		return lostWhen;
	}

	public void setLostWhen(Date lostWhen) {
		this.lostWhen = lostWhen;
	}

	public String getLostWhere() {
		return lostWhere;
	}

	public void setLostWhere(String lostWhere) {
		this.lostWhere = lostWhere;
	}

	public ItemColour getColour() {
		return colour;
	}

	public void setColour(ItemColour colour) {
		this.colour = colour;
	}

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public String getAddDesc() {
		return addDesc;
	}

	public void setAddDesc(String addDesc) {
		this.addDesc = addDesc;
	}

	public ClientTO getReportedByClient() {
		return reportedByClient;
	}

	public void setReportedByClient(ClientTO reportedByClient) {
		this.reportedByClient = reportedByClient;
	}

	public Boolean getIsDealCompleted() {
		return isDealCompleted;
	}

	public void setIsDealCompleted(Boolean isDealCompleted) {
		this.isDealCompleted = isDealCompleted;
	}

	public FoundItemTO getRecognizedAsFoundItem() {
		return recognizedAsFoundItem;
	}

	public void setRecognizedAsFoundItem(FoundItemTO recognizedAsFoundItem) {
		this.recognizedAsFoundItem = recognizedAsFoundItem;
	}

}
