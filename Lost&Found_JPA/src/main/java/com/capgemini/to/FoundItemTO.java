package com.capgemini.to;

import java.sql.Date;

import com.capgemini.enumerator.ItemCategory;
import com.capgemini.enumerator.ItemColour;
import com.capgemini.enumerator.ItemStatus;

public class FoundItemTO extends AbstractTO {

	private static final long serialVersionUID = -4951299089775839393L;

	private String itemName;

	private ItemCategory category;

	private Date foundWhen;

	private String foundWhere;

	private ItemColour colour;

	private Double weight;

	private ItemStatus status;

	private String addDesc;

	private ClientTO reportedByClient;

	private Date outOfDepositSince;

	private ClientTO givenToClient;

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public ItemCategory getCategory() {
		return category;
	}

	public void setCategory(ItemCategory category) {
		this.category = category;
	}

	public Date getFoundWhen() {
		return foundWhen;
	}

	public void setFoundWhen(Date foundWhen) {
		this.foundWhen = foundWhen;
	}

	public String getFoundWhere() {
		return foundWhere;
	}

	public void setFoundWhere(String foundWhere) {
		this.foundWhere = foundWhere;
	}

	public ItemColour getColour() {
		return colour;
	}

	public void setColour(ItemColour colour) {
		this.colour = colour;
	}

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public ItemStatus getStatus() {
		return status;
	}

	public void setStatus(ItemStatus status) {
		this.status = status;
	}

	public String getAddDesc() {
		return addDesc;
	}

	public void setAddDesc(String addDesc) {
		this.addDesc = addDesc;
	}

	public ClientTO getReportedByClient() {
		return reportedByClient;
	}

	public void setReportedByClient(ClientTO reportedByClient) {
		this.reportedByClient = reportedByClient;
	}

	public Date getOutOfDepositSince() {
		return outOfDepositSince;
	}

	public void setOutOfDepositSince(Date outOfDepositSince) {
		this.outOfDepositSince = outOfDepositSince;
	}

	public ClientTO getGivenToClient() {
		return givenToClient;
	}

	public void setGivenToClient(ClientTO givenToClient) {
		this.givenToClient = givenToClient;
	}

}
