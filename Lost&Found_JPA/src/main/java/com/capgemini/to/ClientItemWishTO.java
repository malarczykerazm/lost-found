package com.capgemini.to;

public class ClientItemWishTO extends AbstractTO {

	private static final long serialVersionUID = 8819339699197862648L;

	private ClientTO client;

	private FoundItemTO foundItem;

	private Integer clientRating;

	public ClientTO getClient() {
		return client;
	}

	public void setClient(ClientTO client) {
		this.client = client;
	}

	public FoundItemTO getFoundItem() {
		return foundItem;
	}

	public void setFoundItem(FoundItemTO foundItem) {
		this.foundItem = foundItem;
	}

	public Integer getClientRating() {
		return clientRating;
	}

	public void setClientRating(Integer clientRating) {
		this.clientRating = clientRating;
	}

}