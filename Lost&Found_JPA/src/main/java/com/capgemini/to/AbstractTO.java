package com.capgemini.to;

import java.io.Serializable;
import java.util.Date;

public abstract class AbstractTO implements Serializable {

	private static final long serialVersionUID = 8979113998070399282L;

	private Long id;

	private Long version;

	private Date creationTime;

	private Date modificationTime;

	public Long getID() {
		return id;
	}

	public void setID(Long id) {
		this.id = id;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public Date getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Date creationTime) {
		this.creationTime = creationTime;
	}

	public Date getModificationTime() {
		return modificationTime;
	}

	public void setModificationTime(Date modificationTime) {
		this.modificationTime = modificationTime;
	}

}
