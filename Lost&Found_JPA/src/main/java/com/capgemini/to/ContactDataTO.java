package com.capgemini.to;

public class ContactDataTO extends AbstractTO {

	private static final long serialVersionUID = -4528581440110188076L;

	private String phone;

	private String email;

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
