package com.capgemini.domain;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.capgemini.enumerator.Country;

@Entity
@Table(name = "addresses")
public class AddressEntity extends AbstractEntity {

	private static final long serialVersionUID = -309692233828238728L;

	@NotNull
	@Size(min = 1, max = 5)
	@Column(name = "address_number")
	private String addressNumber;

	@NotNull
	@Size(min = 1, max = 60)
	@Column(name = "street")
	private String street;

	@NotNull
	@Size(min = 1, max = 10)
	@Column(name = "postcode")
	private String postcode;

	@NotNull
	@Size(min = 1, max = 60)
	@Column(name = "city")
	private String city;

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "country")
	private Country country;

	@OneToMany(mappedBy = "address", cascade = { CascadeType.REMOVE, CascadeType.MERGE, CascadeType.DETACH })
	private Set<ClientEntity> clients;

	public String getAddressNumber() {
		return addressNumber;
	}

	public void setAddressNumber(String addressNumber) {
		this.addressNumber = addressNumber;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public Set<ClientEntity> getClients() {
		return clients;
	}

	public void setClients(Set<ClientEntity> clients) {
		this.clients = clients;
	}

	@Override
	protected void deactivateCascadeElements() {
		this.getClients().stream().filter(c -> c.getIsActive() == true).forEach(c -> c.setIsActive(false));
	}

}
