package com.capgemini.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Version;

import org.hibernate.annotations.ColumnDefault;

@MappedSuperclass
public abstract class AbstractEntity implements Serializable {

	private static final long serialVersionUID = 8979113998070399282L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	private Long id;

	@Version
	@ColumnDefault(value = "0")
	@Column(name = "version")
	private Long version;

	@ColumnDefault(value = "CURRENT_TIMESTAMP")
	@Column(name = "creation_time", columnDefinition = "TIMESTAMP", updatable = false)
	private Date creationTime;

	@ColumnDefault(value = "CURRENT_TIMESTAMP")
	@Column(name = "modification_time", columnDefinition = "TIMESTAMP")
	private Date modificationTime;

	@ColumnDefault(value = "TRUE")
	@Column(name = "is_active")
	protected Boolean isActive;

	@Column(name = "deactivation_time")
	protected Date deactivationTime;

	@PrePersist
	public void setDefaultCreationTime() {
		Date creationTime = new Date();
		this.creationTime = creationTime;
		this.setIsActive(true);
		this.setModificationTime(creationTime);
	}

	@PreUpdate
	public void setDefaultModificationTimeAndIsActive() {
		this.modificationTime = new Date();
		if (null == this.isActive) {
			this.isActive = true;
			this.deactivationTime = null;
		}
	}

	public Long getID() {
		return id;
	}

	public void setID(Long id) {
		this.id = id;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public Date getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Date creationTime) {
		this.creationTime = creationTime;
	}

	public Date getModificationTime() {
		return modificationTime;
	}

	public void setModificationTime(Date modificationTime) {
		this.modificationTime = modificationTime;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {

		if (!(isActive) && (this.isActive)) {
			this.deactivationTime = new Date();
			this.isActive = isActive;
			this.deactivateCascadeElements();
		} else if (isActive) {
			this.isActive = isActive;
			this.deactivationTime = null;
		}

	}

	protected abstract void deactivateCascadeElements();

}
