package com.capgemini.domain;

import java.sql.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Range;

import com.capgemini.enumerator.ItemCategory;
import com.capgemini.enumerator.ItemColour;
import com.capgemini.enumerator.ItemStatus;

@Entity
@Table(name = "found_items")
public class FoundItemEntity extends AbstractEntity {

	private static final long serialVersionUID = -6686304699952579869L;

	@NotNull
	@Size(min = 1, max = 70)
	@Column(name = "item_name")
	private String itemName;

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "category")
	private ItemCategory category;

	@NotNull
	@Column(name = "found_when")
	private Date foundWhen;

	@NotNull
	@Size(max = 150)
	@Column(name = "found_where")
	private String foundWhere;

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "colour")
	private ItemColour colour;

	@Range(min = 0)
	@Column(name = "weight")
	private Double weight;

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "status")
	private ItemStatus status;

	@Size(min = 0, max = 250)
	@Column(name = "additional_description")
	private String addDesc;

	@ManyToOne
	@JoinColumn(name = "reported_by_client")
	private ClientEntity reportedByClient;

	@Column(name = "out_of_deposit_since")
	private Date outOfDepositSince;

	@ManyToOne
	@JoinColumn(name = "given_to_client")
	private ClientEntity givenToClient;

	@OneToOne(mappedBy = "recognizedAsFoundItem", cascade = { CascadeType.REMOVE })
	@JoinColumn(name = "recognized_as_lost_item")
	private LostItemEntity recognizedAsLostItem;

	@OneToMany(mappedBy = "foundItem", cascade = { CascadeType.REMOVE })
	private Set<ClientItemWishEntity> clientItemWishes;

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public ItemCategory getCategory() {
		return category;
	}

	public void setCategory(ItemCategory category) {
		this.category = category;
	}

	public Date getFoundWhen() {
		return foundWhen;
	}

	public void setFoundWhen(Date foundWhen) {
		this.foundWhen = foundWhen;
	}

	public String getFoundWhere() {
		return foundWhere;
	}

	public void setFoundWhere(String foundWhere) {
		this.foundWhere = foundWhere;
	}

	public ItemColour getColour() {
		return colour;
	}

	public void setColour(ItemColour colour) {
		this.colour = colour;
	}

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public ItemStatus getStatus() {
		return status;
	}

	public void setStatus(ItemStatus status) {
		this.status = status;
	}

	public String getAddDesc() {
		return addDesc;
	}

	public void setAddDesc(String addDesc) {
		this.addDesc = addDesc;
	}

	public ClientEntity getReportedByClient() {
		return reportedByClient;
	}

	public void setReportedByClient(ClientEntity reportedByClient) {
		this.reportedByClient = reportedByClient;
	}

	public Date getOutOfDepositSince() {
		return outOfDepositSince;
	}

	public void setOutOfDepositSince(Date outOfDepositSince) {
		this.outOfDepositSince = outOfDepositSince;
	}

	public ClientEntity getGivenToClient() {
		return givenToClient;
	}

	public void setGivenToClient(ClientEntity givenToClient) {
		this.givenToClient = givenToClient;
	}

	public LostItemEntity getRecognizedAsLostItem() {
		return recognizedAsLostItem;
	}

	public void setRecognizedAsLostItem(LostItemEntity recognizedAsLostItem) {
		this.recognizedAsLostItem = recognizedAsLostItem;
	}

	public Set<ClientItemWishEntity> getClientItemWishes() {
		return clientItemWishes;
	}

	public void setClientItemWishes(Set<ClientItemWishEntity> clientItemWishes) {
		this.clientItemWishes = clientItemWishes;
	}

	@Override
	protected void deactivateCascadeElements() {
		this.getClientItemWishes().stream().forEach(cIW -> cIW.setIsActive(false));
		if (null != this.getRecognizedAsLostItem()) {
			this.getRecognizedAsLostItem().setIsActive(false);
		}
	}

}
