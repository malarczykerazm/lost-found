package com.capgemini.domain;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;

@Entity
@Table(name = "contact_data")
public class ContactDataEntity extends AbstractEntity {

	private static final long serialVersionUID = 5898126542893426434L;

	@NotNull
	@Size(min = 4, max = 20)
	@Column(name = "phone")
	private String phone;

	@NotNull
	@Size(max = 45)
	@Email
	@Column(name = "email", unique = true)
	private String email;

	@OneToOne(mappedBy = "contactData", cascade = { CascadeType.REMOVE, CascadeType.MERGE, CascadeType.DETACH })
	private ClientEntity client;

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public ClientEntity getClient() {
		return client;
	}

	public void setClient(ClientEntity client) {
		this.client = client;
	}

	@Override
	protected void deactivateCascadeElements() {
		this.getClient().setIsActive(false);
	}

}
