package com.capgemini.domain;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "clients")
public class ClientEntity extends AbstractEntity {

	private static final long serialVersionUID = -7769653744871301381L;

	@NotNull
	@Size(min = 1, max = 20)
	@Column(name = "firstname")
	private String firstname;

	@NotNull
	@Size(min = 1, max = 80)
	@Column(name = "surname")
	private String surname;

	@NotNull
	@Size(min = 11, max = 11)
	@Column(name = "social_security_number")
	private String socialSecurityNumber;

	@NotNull
	@ManyToOne(cascade = { CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REMOVE })
	@JoinColumn(name = "address_id")
	private AddressEntity address;

	@NotNull
	@OneToOne(cascade = { CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REMOVE })
	@JoinColumn(name = "contact_data_id")
	private ContactDataEntity contactData;

	@Size(max = 3)
	@OneToMany(mappedBy = "client", cascade = { CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REMOVE })
	private Set<ClientItemWishEntity> clientItemWishes;

	@OneToMany(mappedBy = "reportedByClient", cascade = { CascadeType.REFRESH, CascadeType.MERGE })
	private Set<FoundItemEntity> foundItemsReported;

	@OneToMany(mappedBy = "reportedByClient", cascade = { CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REMOVE })
	private Set<LostItemEntity> lostItemsReported;

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getSocialSecurityNumber() {
		return socialSecurityNumber;
	}

	public void setSocialSecurityNumber(String socialSecurityNumber) {
		this.socialSecurityNumber = socialSecurityNumber;
	}

	public AddressEntity getAddress() {
		return address;
	}

	public void setAddress(AddressEntity address) {
		this.address = address;
	}

	public ContactDataEntity getContactData() {
		return contactData;
	}

	public void setContactData(ContactDataEntity contactData) {
		this.contactData = contactData;
	}

	public Set<ClientItemWishEntity> getClientItemWishes() {
		return clientItemWishes;
	}

	public void setClientItemWishes(Set<ClientItemWishEntity> clientItemWishes) {
		this.clientItemWishes = clientItemWishes;
	}

	public Set<FoundItemEntity> getFoundItemsReported() {
		return foundItemsReported;
	}

	public void setFoundItemsReported(Set<FoundItemEntity> foundItemsReported) {
		this.foundItemsReported = foundItemsReported;
	}

	public Set<LostItemEntity> getLostItemsReported() {
		return lostItemsReported;
	}

	public void setLostItemsReported(Set<LostItemEntity> lostItemsReported) {
		this.lostItemsReported = lostItemsReported;
	}

	@Override
	protected void deactivateCascadeElements() {
		if (this.getAddress().getIsActive()) {
			long a = this.getAddress().getClients().stream().filter(c -> c.getIsActive() == true).count();
			if (a == 0L) {
				this.getAddress().setIsActive(false);
			}
		}
		if (this.getContactData().getIsActive()) {
			this.getContactData().setIsActive(false);
		}
		this.getClientItemWishes().stream().filter(cIW -> cIW.getIsActive() == true)
				.forEach(cIW -> cIW.setIsActive(false));
		this.getLostItemsReported().stream().filter(lIR -> lIR.getIsActive() == true)
				.forEach(lIR -> lIR.setIsActive(false));
		this.getFoundItemsReported().stream().filter(fIR -> fIR.getIsActive() == true)
				.forEach(fIR -> fIR.setReportedByClient(null));
	}

}
