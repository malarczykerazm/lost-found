package com.capgemini.domain;

import java.sql.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PostPersist;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.validator.constraints.Range;

import com.capgemini.enumerator.ItemCategory;
import com.capgemini.enumerator.ItemColour;

@Entity
@Table(name = "lost_items")
public class LostItemEntity extends AbstractEntity {

	private static final long serialVersionUID = 5378057831320272772L;

	@NotNull
	@Size(min = 1, max = 70)
	@Column(name = "item_name")
	private String itemName;

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "category")
	private ItemCategory category;

	@Column(name = "lost_when")
	private Date lostWhen;

	@Size(min = 0, max = 150)
	@Column(name = "lost_where")
	private String lostWhere;

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "colour")
	private ItemColour colour;

	@Range(min = 0)
	@Column(name = "weight")
	private Double weight;

	@Size(min = 0, max = 250)
	@Column(name = "additional_description")
	private String addDesc;

	@NotNull
	@ManyToOne(cascade = { CascadeType.REFRESH, CascadeType.MERGE })
	@JoinColumn(name = "reported_by_client")
	private ClientEntity reportedByClient;

	@ColumnDefault(value = "FALSE")
	@Column(name = "is_deal_completed")
	private Boolean isDealCompleted;

	@OneToOne(cascade = { CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REMOVE })
	@JoinColumn(name = "recognized_as_found_item")
	private FoundItemEntity recognizedAsFoundItem;

	@PostPersist
	public void setDefaulIsDealCompleted() {
		if (null == this.isDealCompleted) {
			this.isDealCompleted = false;
		}
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public ItemCategory getCategory() {
		return category;
	}

	public void setCategory(ItemCategory category) {
		this.category = category;
	}

	public Date getLostWhen() {
		return lostWhen;
	}

	public void setLostWhen(Date lostWhen) {
		this.lostWhen = lostWhen;
	}

	public String getLostWhere() {
		return lostWhere;
	}

	public void setLostWhere(String lostWhere) {
		this.lostWhere = lostWhere;
	}

	public ItemColour getColour() {
		return colour;
	}

	public void setColour(ItemColour colour) {
		this.colour = colour;
	}

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public String getAddDesc() {
		return addDesc;
	}

	public void setAddDesc(String addDesc) {
		this.addDesc = addDesc;
	}

	public ClientEntity getReportedByClient() {
		return reportedByClient;
	}

	public void setReportedByClient(ClientEntity reportedByClient) {
		this.reportedByClient = reportedByClient;
	}

	public Boolean getIsDealCompleted() {
		return isDealCompleted;
	}

	public void setIsDealCompleted(Boolean isDealCompleted) {
		if (null == isDealCompleted && null == this.isDealCompleted) {
			this.isDealCompleted = false;
		} else if (null != isDealCompleted) {
			this.isDealCompleted = isDealCompleted;
		}
	}

	public FoundItemEntity getRecognizedAsFoundItem() {
		return recognizedAsFoundItem;
	}

	public void setRecognizedAsFoundItem(FoundItemEntity recognizedAsFoundItem) {
		this.recognizedAsFoundItem = recognizedAsFoundItem;
	}

	@Override
	protected void deactivateCascadeElements() {
		if (null != this.getRecognizedAsFoundItem()) {
			this.getRecognizedAsFoundItem().setIsActive(false);
		}
	}

}
