package com.capgemini.domain;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.validator.constraints.Range;

@Entity
@Table(name = "client_item_wishes")
public class ClientItemWishEntity extends AbstractEntity {

	private static final long serialVersionUID = -1020222572748916319L;

	@NotNull
	@ManyToOne(cascade = CascadeType.REFRESH)
	@JoinColumn(name = "client_id")
	private ClientEntity client;

	@NotNull
	@ManyToOne(cascade = CascadeType.REFRESH)
	@JoinColumn(name = "found_item_id")
	private FoundItemEntity foundItem;

	@ColumnDefault(value = "0")
	@Range(min = 0, max = 5)
	@Column(name = "client_rating")
	private Integer clientRating;

	public ClientEntity getClient() {
		return client;
	}

	public void setClient(ClientEntity client) {
		this.client = client;
	}

	public FoundItemEntity getFoundItem() {
		return foundItem;
	}

	public void setFoundItem(FoundItemEntity foundItem) {
		this.foundItem = foundItem;
	}

	public Integer getClientRating() {
		return clientRating;
	}

	public void setClientRating(Integer clientRating) {
		this.clientRating = clientRating;
	}

	@Override
	protected void deactivateCascadeElements() {
	}

}