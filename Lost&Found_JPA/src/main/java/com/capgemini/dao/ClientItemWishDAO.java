package com.capgemini.dao;

import com.capgemini.domain.ClientItemWishEntity;

public interface ClientItemWishDAO extends DAO<ClientItemWishEntity, Long> {

}
