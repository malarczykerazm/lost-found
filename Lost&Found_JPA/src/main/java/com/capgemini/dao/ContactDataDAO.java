package com.capgemini.dao;

import com.capgemini.domain.ContactDataEntity;

public interface ContactDataDAO extends DAO<ContactDataEntity, Long> {

}
