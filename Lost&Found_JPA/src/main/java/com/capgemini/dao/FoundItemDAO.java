package com.capgemini.dao;

import com.capgemini.domain.FoundItemEntity;

public interface FoundItemDAO extends DAO<FoundItemEntity, Long> {

}
