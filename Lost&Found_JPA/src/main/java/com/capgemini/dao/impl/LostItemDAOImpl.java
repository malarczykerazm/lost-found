package com.capgemini.dao.impl;

import org.springframework.stereotype.Repository;

import com.capgemini.dao.LostItemDAO;
import com.capgemini.domain.LostItemEntity;

@Repository
public class LostItemDAOImpl extends AbstractDAO<LostItemEntity, Long> implements LostItemDAO {

}
