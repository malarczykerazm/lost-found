package com.capgemini.dao.impl;

import org.springframework.stereotype.Repository;

import com.capgemini.dao.AddressDAO;
import com.capgemini.domain.AddressEntity;

@Repository
public class AddressDAOImpl extends AbstractDAO<AddressEntity, Long> implements AddressDAO {

}
