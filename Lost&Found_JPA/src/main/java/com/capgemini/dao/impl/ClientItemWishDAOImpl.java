package com.capgemini.dao.impl;

import org.springframework.stereotype.Repository;

import com.capgemini.dao.ClientItemWishDAO;
import com.capgemini.domain.ClientItemWishEntity;

@Repository
public class ClientItemWishDAOImpl extends AbstractDAO<ClientItemWishEntity, Long> implements ClientItemWishDAO {

}
