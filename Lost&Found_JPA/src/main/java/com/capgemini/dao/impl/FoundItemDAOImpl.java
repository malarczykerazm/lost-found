package com.capgemini.dao.impl;

import org.springframework.stereotype.Repository;

import com.capgemini.dao.FoundItemDAO;
import com.capgemini.domain.FoundItemEntity;

@Repository
public class FoundItemDAOImpl extends AbstractDAO<FoundItemEntity, Long> implements FoundItemDAO {

}
