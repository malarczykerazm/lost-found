package com.capgemini.dao.impl;

import org.springframework.stereotype.Repository;

import com.capgemini.dao.ClientDAO;
import com.capgemini.domain.ClientEntity;

@Repository
public class ClientDAOImpl extends AbstractDAO<ClientEntity, Long> implements ClientDAO {

}
