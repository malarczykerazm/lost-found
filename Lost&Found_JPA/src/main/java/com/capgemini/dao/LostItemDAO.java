package com.capgemini.dao;

import com.capgemini.domain.LostItemEntity;

public interface LostItemDAO extends DAO<LostItemEntity, Long> {

}
