package com.capgemini.dao;

import com.capgemini.domain.ClientEntity;

public interface ClientDAO extends DAO<ClientEntity, Long> {

}
