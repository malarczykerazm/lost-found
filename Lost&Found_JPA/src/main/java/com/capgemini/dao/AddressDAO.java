package com.capgemini.dao;

import com.capgemini.domain.AddressEntity;

public interface AddressDAO extends DAO<AddressEntity, Long> {

}
