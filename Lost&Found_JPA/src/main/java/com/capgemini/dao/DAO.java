package com.capgemini.dao;

import java.io.Serializable;
import java.util.List;

import com.capgemini.domain.AbstractEntity;

public interface DAO<T extends AbstractEntity, K extends Serializable> {

	T save(T entity);

	T getOne(K id);

	T findOne(K id);

	List<T> findAll();

	T update(T entity);

	T delete(K id);

	long count();

	T deactivate(K id);

}
