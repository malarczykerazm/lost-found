package com.capgemini.service.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.exception.ElementDeletedException;
import com.capgemini.exception.NoSuchElementInDatabaseException;
import com.capgemini.service.ClientService;
import com.capgemini.service.FoundItemService;
import com.capgemini.to.ClientItemWishTO;
import com.capgemini.to.FoundItemTO;

@RestController
@RequestMapping(value = "/found_items")
public class FoundItemRestService {

	@Autowired
	private FoundItemService foundItemService;

	@Autowired
	private ClientService clientService;

	@CrossOrigin(origins = { "http://localhost:3000", "http://localhost:8080" })
	@RequestMapping(value = "", method = RequestMethod.GET)
	public List<FoundItemTO> findAll() {
		return foundItemService.findAll();
	}

	@CrossOrigin(origins = "http://localhost:3000")
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public FoundItemTO findByID(@PathVariable("id") long id)
			throws NoSuchElementInDatabaseException, ElementDeletedException {
		return foundItemService.findByID(id);
	}

	@CrossOrigin(origins = "http://localhost:3000")
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public FoundItemTO save(@RequestBody FoundItemTO foundItem) {
		if (null != foundItem.getGivenToClient() && null != foundItem.getReportedByClient().getID()) {
			foundItem.setReportedByClient(clientService.findByID(foundItem.getReportedByClient().getID()));
		} else {
			foundItem.setReportedByClient(null);
		}
		if (null != foundItem.getGivenToClient() && null != foundItem.getGivenToClient().getID()) {
			foundItem.setGivenToClient(clientService.findByID(foundItem.getGivenToClient().getID()));
		} else {
			foundItem.setGivenToClient(null);
		}
		return foundItemService.save(foundItem);
	}

	@CrossOrigin(origins = "http://localhost:3000")
	@RequestMapping(value = "/deactivate/{id}", method = RequestMethod.POST)
	public FoundItemTO deactivate(@PathVariable("id") long id)
			throws NoSuchElementInDatabaseException, ElementDeletedException {
		return foundItemService.deactivate(id);
	}

	@CrossOrigin(origins = "http://localhost:3000")
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public FoundItemTO update(@RequestBody FoundItemTO foundItem)
			throws NoSuchElementInDatabaseException, ElementDeletedException {
		if (null != foundItem.getGivenToClient() && null != foundItem.getReportedByClient().getID()) {
			foundItem.setReportedByClient(clientService.findByID(foundItem.getReportedByClient().getID()));
		} else {
			foundItem.setReportedByClient(null);
		}
		if (null != foundItem.getGivenToClient() && null != foundItem.getGivenToClient().getID()) {
			foundItem.setGivenToClient(clientService.findByID(foundItem.getGivenToClient().getID()));
		} else {
			foundItem.setGivenToClient(null);
		}
		return foundItemService.update(foundItem);
	}

	@CrossOrigin(origins = "http://localhost:3000")
	@RequestMapping(value = "/{id}/wishes", method = RequestMethod.GET)
	public List<ClientItemWishTO> getWishes(@PathVariable("id") long id)
			throws NoSuchElementInDatabaseException, ElementDeletedException {
		return foundItemService.findFoundItemWishes(id);
	}

}
