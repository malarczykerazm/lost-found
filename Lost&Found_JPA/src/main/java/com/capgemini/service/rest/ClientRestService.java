package com.capgemini.service.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.exception.ElementDeletedException;
import com.capgemini.exception.NoSuchElementInDatabaseException;
import com.capgemini.service.AddressService;
import com.capgemini.service.ClientService;
import com.capgemini.service.ContactDataService;
import com.capgemini.to.ClientItemWishTO;
import com.capgemini.to.ClientTO;

@RestController
@RequestMapping(value = "/clients")
public class ClientRestService {

	@Autowired
	private ClientService clientService;

	@Autowired
	private AddressService addressService;

	@Autowired
	private ContactDataService contactDataService;

	@CrossOrigin(origins = { "http://localhost:3000", "http://localhost:8080" })
	@RequestMapping(value = "", method = RequestMethod.GET)
	public List<ClientTO> findAll() {
		return clientService.findAll();
	}

	@CrossOrigin(origins = "http://localhost:3000")
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ClientTO findByID(@PathVariable("id") long id)
			throws NoSuchElementInDatabaseException, ElementDeletedException {
		return clientService.findByID(id);
	}

	@CrossOrigin(origins = "http://localhost:3000")
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public ClientTO save(@RequestBody ClientTO client) {
		if (null != client.getAddress().getID()) {
			client.setAddress(addressService.findByID(client.getAddress().getID()));
		} else {
			client.setAddress(addressService.save(client.getAddress()));
		}
		if (null != client.getContactData().getID()) {
			client.setContactData(contactDataService.findByID(client.getContactData().getID()));
		} else {
			client.setContactData(contactDataService.save(client.getContactData()));
		}
		return clientService.save(client);
	}

	@CrossOrigin(origins = "http://localhost:3000")
	@RequestMapping(value = "/deactivate/{id}", method = RequestMethod.POST)
	public ClientTO deactivate(@PathVariable("id") long id)
			throws NoSuchElementInDatabaseException, ElementDeletedException {
		return clientService.deactivate(id);
	}

	@CrossOrigin(origins = "http://localhost:3000")
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ClientTO update(@RequestBody ClientTO client)
			throws NoSuchElementInDatabaseException, ElementDeletedException {
		if (null != client.getAddress().getID()) {
			client.setAddress(addressService.findByID(client.getAddress().getID()));
		} else {
			client.setAddress(addressService.save(client.getAddress()));
		}
		if (null != client.getContactData().getID()) {
			client.setContactData(contactDataService.findByID(client.getContactData().getID()));
		} else {
			client.setContactData(contactDataService.save(client.getContactData()));
		}
		return clientService.update(client);
	}

	@CrossOrigin(origins = "http://localhost:3000")
	@RequestMapping(value = "/{id}/wishes", method = RequestMethod.GET)
	public List<ClientItemWishTO> getWishes(@PathVariable("id") long id)
			throws NoSuchElementInDatabaseException, ElementDeletedException {
		return clientService.findClientsWishes(id);
	}

}
