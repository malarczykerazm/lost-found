package com.capgemini.service.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.exception.ElementDeletedException;
import com.capgemini.exception.NoSuchElementInDatabaseException;
import com.capgemini.service.AddressService;
import com.capgemini.to.AddressTO;

@RestController
@RequestMapping(value = "/addresses")
public class AddressRestService {

	@Autowired
	private AddressService addressService;

	@CrossOrigin(origins = { "http://localhost:3000", "http://localhost:8080" })
	@RequestMapping(value = "", method = RequestMethod.GET)
	public List<AddressTO> findAll() {
		return addressService.findAll();
	}

	@CrossOrigin(origins = "http://localhost:3000")
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public AddressTO findByID(@PathVariable("id") long id)
			throws NoSuchElementInDatabaseException, ElementDeletedException {
		return addressService.findByID(id);
	}

	@CrossOrigin(origins = "http://localhost:3000")
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public AddressTO save(@RequestBody AddressTO address) {
		return addressService.save(address);
	}

	@CrossOrigin(origins = "http://localhost:3000")
	@RequestMapping(value = "/deactivate/{id}", method = RequestMethod.POST)
	public AddressTO deactivate(@PathVariable("id") long id)
			throws NoSuchElementInDatabaseException, ElementDeletedException {
		return addressService.deactivate(id);
	}

	@CrossOrigin(origins = "http://localhost:3000")
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public AddressTO update(@RequestBody AddressTO address)
			throws NoSuchElementInDatabaseException, ElementDeletedException {
		return addressService.update(address);
	}

}
