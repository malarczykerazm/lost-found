package com.capgemini.service.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.service.EnumService;

@RestController
@RequestMapping(value = "/enums")
public class EnumRestService {

	@Autowired
	private EnumService enumService;

	@CrossOrigin(origins = { "http://localhost:3000", "http://localhost:8080" })
	@RequestMapping(value = "/countries", method = RequestMethod.GET)
	public List<String> findAllCountries() {
		return enumService.findAllCountries();
	}

	@CrossOrigin(origins = "http://localhost:3000")
	@RequestMapping(value = "/categories", method = RequestMethod.GET)
	public List<String> findAllCategories() {
		return enumService.findAllCategories();
	}

	@CrossOrigin(origins = "http://localhost:3000")
	@RequestMapping(value = "/colours", method = RequestMethod.GET)
	public List<String> findAllColours() {
		return enumService.findAllColours();
	}

	@CrossOrigin(origins = "http://localhost:3000")
	@RequestMapping(value = "/statuses", method = RequestMethod.GET)
	public List<String> findAllStatuses() {
		return enumService.findAllStatuses();
	}

}
