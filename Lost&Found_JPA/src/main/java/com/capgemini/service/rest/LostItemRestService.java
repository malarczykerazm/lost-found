package com.capgemini.service.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.exception.ElementDeletedException;
import com.capgemini.exception.NoSuchElementInDatabaseException;
import com.capgemini.service.ClientService;
import com.capgemini.service.FoundItemService;
import com.capgemini.service.LostItemService;
import com.capgemini.to.LostItemTO;

@RestController
@RequestMapping(value = "/lost_items")
public class LostItemRestService {

	@Autowired
	private LostItemService lostItemService;

	@Autowired
	private FoundItemService foundItemService;

	@Autowired
	private ClientService clientService;

	@CrossOrigin(origins = { "http://localhost:3000", "http://localhost:8080" })
	@RequestMapping(value = "", method = RequestMethod.GET)
	public List<LostItemTO> findAll() {
		return lostItemService.findAll();
	}

	@CrossOrigin(origins = "http://localhost:3000")
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public LostItemTO findByID(@PathVariable("id") long id)
			throws NoSuchElementInDatabaseException, ElementDeletedException {
		return lostItemService.findByID(id);
	}

	@CrossOrigin(origins = "http://localhost:3000")
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public LostItemTO save(@RequestBody LostItemTO lostItem) {
		if (null != lostItem.getReportedByClient() && null != lostItem.getReportedByClient().getID()) {
			lostItem.setReportedByClient(clientService.findByID(lostItem.getReportedByClient().getID()));
		} else {
			lostItem.setReportedByClient(null);
		}
		if (null != lostItem.getRecognizedAsFoundItem().getID()) {
			lostItem.setRecognizedAsFoundItem(foundItemService.findByID(lostItem.getRecognizedAsFoundItem().getID()));
		} else {
			lostItem.setRecognizedAsFoundItem(null);
		}
		return lostItemService.save(lostItem);
	}

	@CrossOrigin(origins = "http://localhost:3000")
	@RequestMapping(value = "/deactivate/{id}", method = RequestMethod.POST)
	public LostItemTO deactivate(@PathVariable("id") long id)
			throws NoSuchElementInDatabaseException, ElementDeletedException {
		return lostItemService.deactivate(id);
	}

	@CrossOrigin(origins = "http://localhost:3000")
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public LostItemTO update(@RequestBody LostItemTO lostItem)
			throws NoSuchElementInDatabaseException, ElementDeletedException {
		if (null != lostItem.getReportedByClient() && null != lostItem.getReportedByClient().getID()) {
			lostItem.setReportedByClient(clientService.findByID(lostItem.getReportedByClient().getID()));
		} else {
			lostItem.setReportedByClient(null);
		}
		if (null != lostItem.getRecognizedAsFoundItem().getID()) {
			lostItem.setRecognizedAsFoundItem(foundItemService.findByID(lostItem.getRecognizedAsFoundItem().getID()));
		} else {
			lostItem.setRecognizedAsFoundItem(null);
		}
		return lostItemService.update(lostItem);
	}

}
