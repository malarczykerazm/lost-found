package com.capgemini.service.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.exception.ElementDeletedException;
import com.capgemini.exception.NoSuchElementInDatabaseException;
import com.capgemini.exception.TooManyItemWishesOfSingleClientException;
import com.capgemini.exception.TooShortTimeAwaitingException;
import com.capgemini.service.ClientItemWishService;
import com.capgemini.service.ClientService;
import com.capgemini.service.FoundItemService;
import com.capgemini.to.ClientItemWishTO;

@RestController
@RequestMapping(value = "/client_item_wishes")
public class ClientItemWishRestService {

	@Autowired
	private ClientItemWishService clientItemWishService;

	@Autowired
	private ClientService clientService;

	@Autowired
	private FoundItemService foundItemService;

	@CrossOrigin(origins = { "http://localhost:3000", "http://localhost:8080" })
	@RequestMapping(value = "", method = RequestMethod.GET)
	public List<ClientItemWishTO> findAll() {
		return clientItemWishService.findAll();
	}

	@CrossOrigin(origins = "http://localhost:3000")
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ClientItemWishTO findByID(@PathVariable("id") long id)
			throws NoSuchElementInDatabaseException, ElementDeletedException {
		return clientItemWishService.findByID(id);
	}

	@CrossOrigin(origins = "http://localhost:3000")
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public ClientItemWishTO save(@RequestBody ClientItemWishTO clientItemWish)
			throws TooManyItemWishesOfSingleClientException {
		clientItemWish.setClient(clientService.findByID(clientItemWish.getClient().getID()));
		clientItemWish.setFoundItem(foundItemService.findByID(clientItemWish.getFoundItem().getID()));
		return clientItemWishService.save(clientItemWish);
	}

	@CrossOrigin(origins = "http://localhost:3000")
	@RequestMapping(value = "/deactivate/{id}", method = RequestMethod.POST)
	public ClientItemWishTO deactivate(@PathVariable("id") long id)
			throws NoSuchElementInDatabaseException, ElementDeletedException {
		return clientItemWishService.deactivate(id);
	}

	@CrossOrigin(origins = "http://localhost:3000")
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ClientItemWishTO update(@RequestBody ClientItemWishTO clientItemWish)
			throws NoSuchElementInDatabaseException, ElementDeletedException, TooShortTimeAwaitingException {
		clientItemWish.setClient(clientService.findByID(clientItemWish.getClient().getID()));
		clientItemWish.setFoundItem(foundItemService.findByID(clientItemWish.getFoundItem().getID()));
		return clientItemWishService.update(clientItemWish);
	}

}
