package com.capgemini.service.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.exception.ElementDeletedException;
import com.capgemini.exception.NoSuchElementInDatabaseException;
import com.capgemini.service.ContactDataService;
import com.capgemini.to.ContactDataTO;

@RestController
@RequestMapping(value = "/contact_data")
public class ContactDataRestService {

	@Autowired
	private ContactDataService contactDataService;

	@CrossOrigin(origins = "http://localhost:3000")
	@RequestMapping(value = "", method = RequestMethod.GET)
	public List<ContactDataTO> findAll() {
		return contactDataService.findAll();
	}

	@CrossOrigin(origins = { "http://localhost:3000", "http://localhost:8080" })
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ContactDataTO findByID(@PathVariable("id") long id)
			throws NoSuchElementInDatabaseException, ElementDeletedException {
		return contactDataService.findByID(id);
	}

	@CrossOrigin(origins = "http://localhost:3000")
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public ContactDataTO save(@RequestBody ContactDataTO client) {
		return contactDataService.save(client);
	}

	@CrossOrigin(origins = "http://localhost:3000")
	@RequestMapping(value = "/deactivate/{id}", method = RequestMethod.POST)
	public ContactDataTO deactivate(@PathVariable("id") long id)
			throws NoSuchElementInDatabaseException, ElementDeletedException {
		return contactDataService.deactivate(id);
	}

	@CrossOrigin(origins = "http://localhost:3000")
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public ContactDataTO update(@RequestBody ContactDataTO client)
			throws NoSuchElementInDatabaseException, ElementDeletedException {
		return contactDataService.update(client);
	}

}
