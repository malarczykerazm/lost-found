package com.capgemini.service;

import java.util.List;

public interface EnumService {

	List<String> findAllCountries();

	List<String> findAllCategories();

	List<String> findAllColours();

	List<String> findAllStatuses();

}
