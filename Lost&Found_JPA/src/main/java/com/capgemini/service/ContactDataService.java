package com.capgemini.service;

import java.util.List;

import com.capgemini.exception.ElementDeletedException;
import com.capgemini.exception.NoSuchElementInDatabaseException;
import com.capgemini.to.ContactDataTO;

public interface ContactDataService {

	List<ContactDataTO> findAll();

	ContactDataTO findByID(Long id) throws NoSuchElementInDatabaseException, ElementDeletedException;

	ContactDataTO save(ContactDataTO contactData);

	ContactDataTO delete(Long id) throws NoSuchElementInDatabaseException, ElementDeletedException;

	ContactDataTO update(ContactDataTO contactData) throws NoSuchElementInDatabaseException, ElementDeletedException;

	ContactDataTO deactivate(Long id) throws NoSuchElementInDatabaseException, ElementDeletedException;

}
