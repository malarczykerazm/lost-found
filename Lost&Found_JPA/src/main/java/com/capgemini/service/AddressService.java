package com.capgemini.service;

import java.util.List;

import com.capgemini.exception.ElementDeletedException;
import com.capgemini.exception.NoSuchElementInDatabaseException;
import com.capgemini.to.AddressTO;

public interface AddressService {

	List<AddressTO> findAll();

	AddressTO findByID(Long id) throws NoSuchElementInDatabaseException, ElementDeletedException;

	AddressTO save(AddressTO address);

	AddressTO delete(Long id) throws NoSuchElementInDatabaseException, ElementDeletedException;

	AddressTO update(AddressTO address) throws NoSuchElementInDatabaseException, ElementDeletedException;

	AddressTO deactivate(Long id) throws NoSuchElementInDatabaseException, ElementDeletedException;

}
