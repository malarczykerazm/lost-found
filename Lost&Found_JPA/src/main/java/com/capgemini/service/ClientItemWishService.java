package com.capgemini.service;

import java.util.List;

import com.capgemini.exception.ElementDeletedException;
import com.capgemini.exception.NoSuchElementInDatabaseException;
import com.capgemini.exception.TooShortTimeAwaitingException;
import com.capgemini.to.ClientItemWishTO;

public interface ClientItemWishService {

	List<ClientItemWishTO> findAll();

	ClientItemWishTO findByID(Long id) throws NoSuchElementInDatabaseException, ElementDeletedException;

	ClientItemWishTO save(ClientItemWishTO clientItemWish) throws TooShortTimeAwaitingException;

	ClientItemWishTO delete(Long id) throws NoSuchElementInDatabaseException, ElementDeletedException;

	ClientItemWishTO update(ClientItemWishTO clientItemWish)
			throws NoSuchElementInDatabaseException, ElementDeletedException, TooShortTimeAwaitingException;

	ClientItemWishTO deactivate(Long id) throws NoSuchElementInDatabaseException, ElementDeletedException;

}
