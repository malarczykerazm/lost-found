package com.capgemini.service;

import java.util.List;

import com.capgemini.exception.ElementDeletedException;
import com.capgemini.exception.NoSuchElementInDatabaseException;
import com.capgemini.to.ClientItemWishTO;
import com.capgemini.to.ClientTO;

public interface ClientService {

	List<ClientTO> findAll();

	ClientTO findByID(Long id) throws NoSuchElementInDatabaseException, ElementDeletedException;

	ClientTO save(ClientTO client);

	ClientTO delete(Long id) throws NoSuchElementInDatabaseException, ElementDeletedException;

	ClientTO update(ClientTO client) throws NoSuchElementInDatabaseException, ElementDeletedException;

	ClientTO deactivate(Long id) throws NoSuchElementInDatabaseException, ElementDeletedException;

	List<ClientItemWishTO> findClientsWishes(Long id) throws NoSuchElementInDatabaseException, ElementDeletedException;

}
