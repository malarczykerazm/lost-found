package com.capgemini.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.capgemini.dao.LostItemDAO;
import com.capgemini.domain.LostItemEntity;
import com.capgemini.exception.ElementDeletedException;
import com.capgemini.exception.NoSuchElementInDatabaseException;
import com.capgemini.mapper.LostItemMapper;
import com.capgemini.service.LostItemService;
import com.capgemini.service.validation.CommonValidationService;
import com.capgemini.to.LostItemTO;

@Service
@Transactional(readOnly = true)
public class LostItemServiceImpl implements LostItemService {

	@Autowired
	private LostItemDAO lostItemRepository;

	@Autowired
	private CommonValidationService<LostItemEntity> commonValidation;

	@Override
	public List<LostItemTO> findAll() {
		return lostItemRepository.findAll().stream().filter(li -> li.getIsActive() == true)
				.map(li -> LostItemMapper.map(li)).collect(Collectors.toList());
	}

	@Override
	public LostItemTO findByID(Long id) throws NoSuchElementInDatabaseException, ElementDeletedException {
		commonValidation.validateIfAnythingActiveWasFound(LostItemEntity.class, id);
		return LostItemMapper.map(lostItemRepository.findOne(id));
	}

	@Override
	@Transactional(readOnly = false)
	public LostItemTO save(LostItemTO lostItem) {
		return LostItemMapper.map(lostItemRepository.save(LostItemMapper.map(lostItem)));
	}

	@Override
	@Transactional(readOnly = false)
	public LostItemTO delete(Long id) throws NoSuchElementInDatabaseException, ElementDeletedException {
		commonValidation.validateIfAnythingActiveWasFound(LostItemEntity.class, id);
		return LostItemMapper.map(lostItemRepository.delete(id));
	}

	@Override
	@Transactional(readOnly = false)
	public LostItemTO update(LostItemTO lostItem) throws NoSuchElementInDatabaseException, ElementDeletedException {
		commonValidation.validateObjectInputForUpdate(LostItemMapper.map(lostItem));
		return LostItemMapper.map(lostItemRepository.update(LostItemMapper.map(lostItem)));
	}

	@Override
	@Transactional(readOnly = false)
	public LostItemTO deactivate(Long id) throws NoSuchElementInDatabaseException, ElementDeletedException {
		commonValidation.validateIfAnythingActiveWasFound(LostItemEntity.class, id);
		return LostItemMapper.map(lostItemRepository.deactivate(id));
	}

}