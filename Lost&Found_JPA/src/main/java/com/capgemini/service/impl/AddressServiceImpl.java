package com.capgemini.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.capgemini.dao.AddressDAO;
import com.capgemini.domain.AddressEntity;
import com.capgemini.exception.ElementDeletedException;
import com.capgemini.exception.NoSuchElementInDatabaseException;
import com.capgemini.mapper.AddressMapper;
import com.capgemini.service.AddressService;
import com.capgemini.service.validation.CommonValidationService;
import com.capgemini.to.AddressTO;

@Service
@Transactional(readOnly = true)
public class AddressServiceImpl implements AddressService {

	@Autowired
	private AddressDAO addressRepository;

	@Autowired
	private CommonValidationService<AddressEntity> commonValidation;

	@Override
	public List<AddressTO> findAll() {
		return addressRepository.findAll().stream().filter(a -> a.getIsActive() == true).map(a -> AddressMapper.map(a))
				.collect(Collectors.toList());
	}

	@Override
	public AddressTO findByID(Long id) throws NoSuchElementInDatabaseException, ElementDeletedException {
		commonValidation.validateIfAnythingActiveWasFound(AddressEntity.class, id);
		return AddressMapper.map(addressRepository.findOne(id));
	}

	@Override
	@Transactional(readOnly = false)
	public AddressTO save(AddressTO address) {
		return AddressMapper.map(addressRepository.save(AddressMapper.map(address)));
	}

	@Override
	@Transactional(readOnly = false)
	public AddressTO delete(Long id) throws NoSuchElementInDatabaseException, ElementDeletedException {
		commonValidation.validateIfAnythingActiveWasFound(AddressEntity.class, id);
		return AddressMapper.map(addressRepository.delete(id));
	}

	@Override
	@Transactional(readOnly = false)
	public AddressTO update(AddressTO address) throws NoSuchElementInDatabaseException, ElementDeletedException {
		commonValidation.validateObjectInputForUpdate(AddressMapper.map(address));
		return AddressMapper.map(addressRepository.update(AddressMapper.map(address)));
	}

	@Override
	@Transactional(readOnly = false)
	public AddressTO deactivate(Long id) throws NoSuchElementInDatabaseException, ElementDeletedException {
		commonValidation.validateIfAnythingActiveWasFound(AddressEntity.class, id);
		return AddressMapper.map(addressRepository.deactivate(id));
	}

}