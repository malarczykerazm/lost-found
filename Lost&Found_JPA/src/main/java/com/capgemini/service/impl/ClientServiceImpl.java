package com.capgemini.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.capgemini.dao.ClientDAO;
import com.capgemini.domain.ClientEntity;
import com.capgemini.exception.ElementDeletedException;
import com.capgemini.exception.NoSuchElementInDatabaseException;
import com.capgemini.mapper.ClientItemWishMapper;
import com.capgemini.mapper.ClientMapper;
import com.capgemini.service.ClientService;
import com.capgemini.service.validation.CommonValidationService;
import com.capgemini.to.ClientItemWishTO;
import com.capgemini.to.ClientTO;

@Service
@Transactional(readOnly = true)
public class ClientServiceImpl implements ClientService {

	@Autowired
	private ClientDAO clientRepository;

	@Autowired
	private CommonValidationService<ClientEntity> commonValidation;

	@Override
	public List<ClientTO> findAll() {
		return clientRepository.findAll().stream().filter(c -> c.getIsActive() == true).map(c -> ClientMapper.map(c))
				.collect(Collectors.toList());
	}

	@Override
	public ClientTO findByID(Long id) throws NoSuchElementInDatabaseException, ElementDeletedException {
		commonValidation.validateIfAnythingActiveWasFound(ClientEntity.class, id);
		return ClientMapper.map(clientRepository.findOne(id));
	}

	@Override
	@Transactional(readOnly = false)
	public ClientTO save(ClientTO client) {
		return ClientMapper.map(clientRepository.save(ClientMapper.map(client)));
	}

	@Override
	@Transactional(readOnly = false)
	public ClientTO delete(Long id) throws NoSuchElementInDatabaseException, ElementDeletedException {
		commonValidation.validateIfAnythingActiveWasFound(ClientEntity.class, id);
		return ClientMapper.map(clientRepository.delete(id));
	}

	@Override
	@Transactional(readOnly = false)
	public ClientTO update(ClientTO client) throws NoSuchElementInDatabaseException, ElementDeletedException {
		commonValidation.validateObjectInputForUpdate(ClientMapper.map(client));
		return ClientMapper.map(clientRepository.update(ClientMapper.map(client)));
	}

	@Override
	@Transactional(readOnly = false)
	public ClientTO deactivate(Long id) throws NoSuchElementInDatabaseException, ElementDeletedException {
		commonValidation.validateIfAnythingActiveWasFound(ClientEntity.class, id);
		return ClientMapper.map(clientRepository.deactivate(id));
	}

	@Override
	public List<ClientItemWishTO> findClientsWishes(Long id)
			throws NoSuchElementInDatabaseException, ElementDeletedException {
		commonValidation.validateIfAnythingActiveWasFound(ClientEntity.class, id);
		return clientRepository.findOne(id).getClientItemWishes().stream().filter(cIW -> cIW.getIsActive() == true)
				.map(cIW -> ClientItemWishMapper.map(cIW)).collect(Collectors.toList());
	}

}