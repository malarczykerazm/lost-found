package com.capgemini.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.capgemini.dao.ContactDataDAO;
import com.capgemini.domain.ContactDataEntity;
import com.capgemini.exception.ElementDeletedException;
import com.capgemini.exception.NoSuchElementInDatabaseException;
import com.capgemini.mapper.ContactDataMapper;
import com.capgemini.service.ContactDataService;
import com.capgemini.service.validation.CommonValidationService;
import com.capgemini.to.ContactDataTO;

@Service
@Transactional(readOnly = true)
public class ContactDataServiceImpl implements ContactDataService {

	@Autowired
	private ContactDataDAO contactDataRepository;

	@Autowired
	private CommonValidationService<ContactDataEntity> commonValidation;

	@Override
	public List<ContactDataTO> findAll() {
		return contactDataRepository.findAll().stream().filter(cd -> cd.getIsActive() == true)
				.map(cd -> ContactDataMapper.map(cd)).collect(Collectors.toList());
	}

	@Override
	public ContactDataTO findByID(Long id) throws NoSuchElementInDatabaseException, ElementDeletedException {
		commonValidation.validateIfAnythingActiveWasFound(ContactDataEntity.class, id);
		return ContactDataMapper.map(contactDataRepository.findOne(id));
	}

	@Override
	@Transactional(readOnly = false)
	public ContactDataTO save(ContactDataTO contactData) {
		return ContactDataMapper.map(contactDataRepository.save(ContactDataMapper.map(contactData)));
	}

	@Override
	@Transactional(readOnly = false)
	public ContactDataTO delete(Long id) throws NoSuchElementInDatabaseException, ElementDeletedException {
		commonValidation.validateIfAnythingActiveWasFound(ContactDataEntity.class, id);
		return ContactDataMapper.map(contactDataRepository.delete(id));
	}

	@Override
	@Transactional(readOnly = false)
	public ContactDataTO update(ContactDataTO contactData)
			throws NoSuchElementInDatabaseException, ElementDeletedException {
		commonValidation.validateObjectInputForUpdate(ContactDataMapper.map(contactData));
		return ContactDataMapper.map(contactDataRepository.update(ContactDataMapper.map(contactData)));
	}

	@Override
	@Transactional(readOnly = false)
	public ContactDataTO deactivate(Long id) throws NoSuchElementInDatabaseException, ElementDeletedException {
		commonValidation.validateIfAnythingActiveWasFound(ContactDataEntity.class, id);
		return ContactDataMapper.map(contactDataRepository.deactivate(id));
	}

}