package com.capgemini.service.impl;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.capgemini.enumerator.Country;
import com.capgemini.enumerator.ItemCategory;
import com.capgemini.enumerator.ItemColour;
import com.capgemini.enumerator.ItemStatus;
import com.capgemini.service.EnumService;

@Service
@Transactional(readOnly = true)
public class EnumServiceImpl implements EnumService {

	@Override
	public List<String> findAllCountries() {
		Country enumValue = Country.AFGHANISTAN;
		return Arrays.asList(enumValue.getDeclaringClass().getEnumConstants()).stream().map(c -> c.toString())
				.collect(Collectors.toList());
	}

	@Override
	public List<String> findAllCategories() {
		ItemCategory enumValue = ItemCategory.BAG;
		return Arrays.asList(enumValue.getDeclaringClass().getEnumConstants()).stream().map(cat -> cat.toString())
				.collect(Collectors.toList());
	}

	@Override
	public List<String> findAllColours() {
		ItemColour enumValue = ItemColour.BLACK;
		return Arrays.asList(enumValue.getDeclaringClass().getEnumConstants()).stream().map(col -> col.toString())
				.collect(Collectors.toList());
	}

	@Override
	public List<String> findAllStatuses() {
		ItemStatus enumValue = ItemStatus.IN_DEPOSIT;
		return Arrays.asList(enumValue.getDeclaringClass().getEnumConstants()).stream().map(s -> s.toString())
				.collect(Collectors.toList());
	}

}