package com.capgemini.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.capgemini.dao.FoundItemDAO;
import com.capgemini.domain.FoundItemEntity;
import com.capgemini.exception.ElementDeletedException;
import com.capgemini.exception.NoSuchElementInDatabaseException;
import com.capgemini.mapper.ClientItemWishMapper;
import com.capgemini.mapper.FoundItemMapper;
import com.capgemini.service.FoundItemService;
import com.capgemini.service.validation.CommonValidationService;
import com.capgemini.to.ClientItemWishTO;
import com.capgemini.to.FoundItemTO;

@Service
@Transactional(readOnly = true)
public class FoundItemServiceImpl implements FoundItemService {

	@Autowired
	private FoundItemDAO foundItemRepository;

	@Autowired
	private CommonValidationService<FoundItemEntity> commonValidation;

	@Override
	public List<FoundItemTO> findAll() {
		return foundItemRepository.findAll().stream().filter(fi -> fi.getIsActive() == true)
				.map(fi -> FoundItemMapper.map(fi)).collect(Collectors.toList());
	}

	@Override
	public FoundItemTO findByID(Long id) throws NoSuchElementInDatabaseException, ElementDeletedException {
		commonValidation.validateIfAnythingActiveWasFound(FoundItemEntity.class, id);
		return FoundItemMapper.map(foundItemRepository.findOne(id));
	}

	@Override
	@Transactional(readOnly = false)
	public FoundItemTO save(FoundItemTO foundItem) {
		return FoundItemMapper.map(foundItemRepository.save(FoundItemMapper.map(foundItem)));
	}

	@Override
	@Transactional(readOnly = false)
	public FoundItemTO delete(Long id) throws NoSuchElementInDatabaseException, ElementDeletedException {
		commonValidation.validateIfAnythingActiveWasFound(FoundItemEntity.class, id);
		return FoundItemMapper.map(foundItemRepository.delete(id));
	}

	@Override
	@Transactional(readOnly = false)
	public FoundItemTO update(FoundItemTO foundItem) throws NoSuchElementInDatabaseException, ElementDeletedException {
		commonValidation.validateObjectInputForUpdate(FoundItemMapper.map(foundItem));
		return FoundItemMapper.map(foundItemRepository.update(FoundItemMapper.map(foundItem)));
	}

	@Override
	@Transactional(readOnly = false)
	public FoundItemTO deactivate(Long id) throws NoSuchElementInDatabaseException, ElementDeletedException {
		commonValidation.validateIfAnythingActiveWasFound(FoundItemEntity.class, id);
		return FoundItemMapper.map(foundItemRepository.deactivate(id));
	}

	@Override
	public List<ClientItemWishTO> findFoundItemWishes(Long id)
			throws NoSuchElementInDatabaseException, ElementDeletedException {
		commonValidation.validateIfAnythingActiveWasFound(FoundItemEntity.class, id);
		return foundItemRepository.findOne(id).getClientItemWishes().stream().filter(cIW -> cIW.getIsActive() == true)
				.map(cIW -> ClientItemWishMapper.map(cIW)).collect(Collectors.toList());
	}

}