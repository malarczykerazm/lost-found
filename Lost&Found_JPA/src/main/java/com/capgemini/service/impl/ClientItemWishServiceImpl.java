package com.capgemini.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.capgemini.dao.ClientItemWishDAO;
import com.capgemini.dao.FoundItemDAO;
import com.capgemini.domain.ClientItemWishEntity;
import com.capgemini.enumerator.ItemStatus;
import com.capgemini.exception.ElementDeletedException;
import com.capgemini.exception.NoSuchElementInDatabaseException;
import com.capgemini.exception.TooManyItemWishesOfSingleClientException;
import com.capgemini.exception.TooShortTimeAwaitingException;
import com.capgemini.mapper.ClientItemWishMapper;
import com.capgemini.service.ClientItemWishService;
import com.capgemini.service.validation.ClientItemWishValidationService;
import com.capgemini.service.validation.CommonValidationService;
import com.capgemini.to.ClientItemWishTO;

@Service
@Transactional(readOnly = true)
public class ClientItemWishServiceImpl implements ClientItemWishService {

	@Autowired
	private ClientItemWishDAO clientItemWishRepository;

	@Autowired
	private FoundItemDAO foundItemRepository;

	@Autowired
	private CommonValidationService<ClientItemWishEntity> commonValidation;

	@Autowired
	private ClientItemWishValidationService clientItemWishValidation;

	@Override
	public List<ClientItemWishTO> findAll() {
		return clientItemWishRepository.findAll().stream().filter(cIW -> cIW.getIsActive() == true)
				.map(cIW -> ClientItemWishMapper.map(cIW)).collect(Collectors.toList());
	}

	@Override
	public ClientItemWishTO findByID(Long id) throws NoSuchElementInDatabaseException, ElementDeletedException {
		commonValidation.validateIfAnythingActiveWasFound(ClientItemWishEntity.class, id);
		return ClientItemWishMapper.map(clientItemWishRepository.findOne(id));
	}

	@Override
	@Transactional(readOnly = false)
	public ClientItemWishTO save(ClientItemWishTO clientItemWish) throws TooManyItemWishesOfSingleClientException {
		clientItemWishValidation.validate3ItemsRule(clientItemWish.getClient());
		return ClientItemWishMapper.map(clientItemWishRepository.save(ClientItemWishMapper.map(clientItemWish)));
	}

	@Override
	@Transactional(readOnly = false)
	public ClientItemWishTO delete(Long id) throws NoSuchElementInDatabaseException, ElementDeletedException {
		commonValidation.validateIfAnythingActiveWasFound(ClientItemWishEntity.class, id);
		return ClientItemWishMapper.map(clientItemWishRepository.delete(id));
	}

	@Override
	@Transactional(readOnly = false)
	public ClientItemWishTO update(ClientItemWishTO clientItemWish)
			throws NoSuchElementInDatabaseException, ElementDeletedException, TooShortTimeAwaitingException {
		commonValidation.validateObjectInputForUpdate(ClientItemWishMapper.map(clientItemWish));
		if (ItemStatus.IN_DEPOSIT == foundItemRepository.findOne(clientItemWish.getFoundItem().getID()).getStatus()
				&& ItemStatus.IN_DEPOSIT != clientItemWish.getFoundItem().getStatus()) {
			clientItemWishValidation.validateIfItemAwaitingTimeIsLongEnough(clientItemWish.getFoundItem());
		}
		return ClientItemWishMapper.map(clientItemWishRepository.update(ClientItemWishMapper.map(clientItemWish)));
	}

	@Override
	@Transactional(readOnly = false)
	public ClientItemWishTO deactivate(Long id) throws NoSuchElementInDatabaseException, ElementDeletedException {
		commonValidation.validateIfAnythingActiveWasFound(ClientItemWishEntity.class, id);
		return ClientItemWishMapper.map(clientItemWishRepository.deactivate(id));
	}

}