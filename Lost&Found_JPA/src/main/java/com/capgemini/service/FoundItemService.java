package com.capgemini.service;

import java.util.List;

import com.capgemini.exception.ElementDeletedException;
import com.capgemini.exception.NoSuchElementInDatabaseException;
import com.capgemini.to.ClientItemWishTO;
import com.capgemini.to.FoundItemTO;

public interface FoundItemService {

	List<FoundItemTO> findAll();

	FoundItemTO findByID(Long id) throws NoSuchElementInDatabaseException, ElementDeletedException;

	FoundItemTO save(FoundItemTO foundItem);

	FoundItemTO delete(Long id) throws NoSuchElementInDatabaseException, ElementDeletedException;

	FoundItemTO update(FoundItemTO foundItem) throws NoSuchElementInDatabaseException, ElementDeletedException;

	FoundItemTO deactivate(Long id) throws NoSuchElementInDatabaseException, ElementDeletedException;

	List<ClientItemWishTO> findFoundItemWishes(Long id)
			throws NoSuchElementInDatabaseException, ElementDeletedException;

}
