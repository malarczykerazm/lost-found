package com.capgemini.service;

import java.util.List;

import com.capgemini.exception.ElementDeletedException;
import com.capgemini.exception.NoSuchElementInDatabaseException;
import com.capgemini.to.LostItemTO;

public interface LostItemService {

	List<LostItemTO> findAll();

	LostItemTO findByID(Long id) throws NoSuchElementInDatabaseException, ElementDeletedException;

	LostItemTO save(LostItemTO lostItem);

	LostItemTO delete(Long id) throws NoSuchElementInDatabaseException, ElementDeletedException;

	LostItemTO update(LostItemTO lostItem) throws NoSuchElementInDatabaseException, ElementDeletedException;

	LostItemTO deactivate(Long id) throws NoSuchElementInDatabaseException, ElementDeletedException;

}
