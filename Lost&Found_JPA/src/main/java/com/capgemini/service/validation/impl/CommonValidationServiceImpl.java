package com.capgemini.service.validation.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.capgemini.domain.AbstractEntity;
import com.capgemini.exception.ElementDeletedException;
import com.capgemini.exception.NoSuchElementInDatabaseException;
import com.capgemini.service.validation.CommonValidationService;

@Service
@Transactional(readOnly = true)
public class CommonValidationServiceImpl<T extends AbstractEntity> implements CommonValidationService<T> {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public void validateIfAnythingActiveWasFound(Class<T> clazz, Long parameter)
			throws NoSuchElementInDatabaseException, ElementDeletedException {
		T foundElement = entityManager.find(clazz, parameter);
		if (null == foundElement) {
			throw new NoSuchElementInDatabaseException(
					"There was no search results for the provided ID of " + parameter + ".");
		}
		if (false == foundElement.getIsActive()) {
			throw new ElementDeletedException("The element you're looking for has been deleted.");
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public void validateObjectInputForUpdate(T input) throws NoSuchElementInDatabaseException, ElementDeletedException {
		if (null == input) {
			throw new NoSuchElementInDatabaseException("There was no valid input.");
		}
		if (null == input.getID()) {
			throw new NoSuchElementInDatabaseException("There was no valid input. The input has no ID.");
		}
		T foundElement = (T) entityManager.find(input.getClass(), input.getID());
		if (null == foundElement) {
			throw new NoSuchElementInDatabaseException(
					"There is no element in the database to process. Element ID not found.");
		}
		if (false == foundElement.getIsActive()) {
			throw new ElementDeletedException("The element you're looking for has been deleted.");
		}
	}

}
