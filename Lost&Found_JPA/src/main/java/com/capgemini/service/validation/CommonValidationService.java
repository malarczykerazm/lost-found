package com.capgemini.service.validation;

import com.capgemini.exception.ElementDeletedException;
import com.capgemini.exception.NoSuchElementInDatabaseException;

public interface CommonValidationService<T> {

	public void validateIfAnythingActiveWasFound(Class<T> clazz, Long parameter)
			throws NoSuchElementInDatabaseException, ElementDeletedException;

	public void validateObjectInputForUpdate(T input) throws NoSuchElementInDatabaseException, ElementDeletedException;

}
