package com.capgemini.service.validation.impl;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.capgemini.dao.ClientItemWishDAO;
import com.capgemini.dao.FoundItemDAO;
import com.capgemini.enumerator.ItemStatus;
import com.capgemini.exception.TooManyItemWishesOfSingleClientException;
import com.capgemini.exception.TooShortTimeAwaitingException;
import com.capgemini.service.validation.ClientItemWishValidationService;
import com.capgemini.to.ClientTO;
import com.capgemini.to.FoundItemTO;

@Service
@Transactional(readOnly = true)
public class ClientItemWishValidationServiceImpl implements ClientItemWishValidationService {

	@Autowired
	private ClientItemWishDAO clientItemWishRepository;

	@Autowired
	private FoundItemDAO foundItemRepository;

	public void validate3ItemsRule(ClientTO client) throws TooManyItemWishesOfSingleClientException {
		long numberOfClientItemWishes = clientItemWishRepository.findAll().stream()
				.filter(cIW -> (cIW.getIsActive() == true) && (cIW.getFoundItem().getStatus() == ItemStatus.IN_DEPOSIT)
						&& (cIW.getClient().getID() == client.getID()))
				.count();

		if (numberOfClientItemWishes >= 3L) {
			throw new TooManyItemWishesOfSingleClientException(
					"Chosen client already reached the maximum number of item wishes. Remove one of them to add another.");
		}
	}

	@Override
	public void validateIfItemAwaitingTimeIsLongEnough(FoundItemTO foundItem) throws TooShortTimeAwaitingException {
		if (getDateDiff(foundItemRepository.findOne(foundItem.getID()).getCreationTime(), new Date(),
				TimeUnit.MINUTES) < 1) {
			throw new TooShortTimeAwaitingException("It's too soon to crate a wish queue for this item.");
		}
	}

	private static long getDateDiff(Date startDate, Date endDate, TimeUnit timeUnit) {
		long diffInMillies = endDate.getTime() - startDate.getTime();
		return timeUnit.convert(diffInMillies, TimeUnit.MILLISECONDS);
	}

}
