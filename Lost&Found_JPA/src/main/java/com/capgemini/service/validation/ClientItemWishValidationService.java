package com.capgemini.service.validation;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.capgemini.exception.TooManyItemWishesOfSingleClientException;
import com.capgemini.exception.TooShortTimeAwaitingException;
import com.capgemini.to.ClientTO;
import com.capgemini.to.FoundItemTO;

@Service
@Transactional(readOnly = true)
public interface ClientItemWishValidationService {

	void validate3ItemsRule(ClientTO client) throws TooManyItemWishesOfSingleClientException;

	void validateIfItemAwaitingTimeIsLongEnough(FoundItemTO foundItem) throws TooShortTimeAwaitingException;

}
