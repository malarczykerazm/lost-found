package com.capgemini.enumerator;

public enum ItemStatus {
	IN_DEPOSIT, RETURNED_TO_OWNER, GIVEN_AWAY
}
