package com.capgemini.enumerator;

public enum ItemColour {
	BLACK, BLUE, BROWN, GOLDEN, GRAY, GREEN, ORANGE, PINK, PURPLE, RED, WHITE, YELLOW
}
