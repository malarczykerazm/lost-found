package com.capgemini.enumerator;

public enum ItemCategory {
	BAG, BIKE, BOOK, CLOTHES, CREDIT_OR_DEBIT_CARD, DOCUMENT, ELECTRONICS, PERSONAL_ITEM, UMBRELLA, WALLET, OTHER
}
