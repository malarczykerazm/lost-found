package com.capgemini.exception;

public class TooShortTimeAwaitingException extends RuntimeException {

	private static final long serialVersionUID = -5711554031376555591L;

	public TooShortTimeAwaitingException(String message) {
		super(message);
	}

}
