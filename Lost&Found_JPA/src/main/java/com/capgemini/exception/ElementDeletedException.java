package com.capgemini.exception;

public class ElementDeletedException extends RuntimeException {

	private static final long serialVersionUID = -6417079779586763677L;

	public ElementDeletedException(String message) {
		super(message);
	}

}
