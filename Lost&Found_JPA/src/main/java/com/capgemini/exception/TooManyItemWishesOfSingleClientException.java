package com.capgemini.exception;

public class TooManyItemWishesOfSingleClientException extends RuntimeException {

	private static final long serialVersionUID = -6417079779586763677L;

	public TooManyItemWishesOfSingleClientException(String message) {
		super(message);
	}

}
