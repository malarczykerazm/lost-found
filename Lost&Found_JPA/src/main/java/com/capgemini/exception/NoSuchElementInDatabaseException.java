package com.capgemini.exception;

public class NoSuchElementInDatabaseException extends RuntimeException {

	private static final long serialVersionUID = -7981582115925405889L;

	public NoSuchElementInDatabaseException(String message) {
		super(message);
	}

}
